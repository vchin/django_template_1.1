import settings
LOCAL = True
DEBUG = True
TEMPLATE_DEBUG = DEBUG
THUMBNAIL_DEBUG = True
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'reswres'
EMAIL_HOST_PASSWORD = 'tempotempo'
EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = 'reswres@yandex.ru'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'db{{ cookiecutter.repo_name }}', # Or path to database file if using sqlite3.
        'USER': 'root', # Not used with sqlite3.
        'PASSWORD': 'root', # Not used with sqlite3.
        'HOST': '', # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '', # Set to empty string for default. Not used with sqlite3.
        'OPTIONS': {'init_command': 'SET storage_engine=MYISAM;'}

    }
}

TEMPLATE_DIRS = (
    '%s/templates' % settings.PROJECT_ROOT,
)

