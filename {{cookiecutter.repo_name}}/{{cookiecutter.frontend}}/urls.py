from django.conf.urls import patterns, include, url
from django.views.decorators.csrf import csrf_exempt
from views import ViewIndex

urlpatterns = patterns('',
    url(r'^$', csrf_exempt(ViewIndex.as_view()), name='view_index'),
)

