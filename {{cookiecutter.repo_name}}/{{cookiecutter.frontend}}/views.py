from common.views import MixinBase
from django.views.generic import TemplateView

import logging

logger = logging.getLogger('{{ cookiecutter.repo_name }}.views')


class ViewIndex(MixinBase, TemplateView):
    pass