# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

import logging

logger = logging.getLogger('{{ cookiecutter.repo_name }}.models')

# Create your models here.
