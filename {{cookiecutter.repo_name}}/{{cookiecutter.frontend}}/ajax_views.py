# -*- coding: utf-8 -*-
from common.views import MixinBase, AjaxView
import logging
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger('{{ cookiecutter.repo_name }}.views')


class ViewIndex(AjaxView):
    def cmd_init(self, request):
        return 'ok'

